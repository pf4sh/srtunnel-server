# srtunnel-server

Server part to control clients with portforwarding.

## system overview

Find Server and User-Station parts in this repo.
![](visual-overview.png)

## setup gw-n

Create a folder `/etc/srtunnel` with write-access to the group where
`gwuser, gwdata, gwsync` is member

To update the keys from master add this via `incrontab -e`:

```
/home/gwdata/.ssh/authorized_keys IN_CLOSE_WRITE /home/gwdata/bin/lsync
/etc/srtunnel/remote_authorized_keys IN_CLOSE_WRITE /home/gwdata/bin/bsync
```

for user `gwdata`.

## setup master-config

Create a folder `/etc/srtunnel` with write-access for user `gwsync`. This
folder contains the master-config for all clients.

To synchronize to all gw-n, setup cron via crontab -e an ad this lines:

```
*/4 * * * * $HOME/bin/conf_sync
*/4 * * * * $HOME/bin/sync_keys > /dev/null
```

This will distribute the HASH-based configs and make the registered clients
visible on all gw-n hosts.

## client config-file.yml

The exported services will configured by the client-config file and exposed to
the given TCP-Ports on the gw-n host.

```
config:
  alias: client-name
name: sha1-client-hash
rports:
  RHTTP: int-gt-1024
  RSSH: int-gt-RHTTP
ts: utc-ts
```

Please use a proper service to generate the file and don't overlap the exposed
ports.

```
port = int(int(client-hash, 16) / int(40*"f", 16) * int("ffff", 16) * 0.98435) + 1025
```
