#!/usr/bin/python3
import os
import sys


def read(path, erg_dict):
    if os.path.isfile(path):
        raw = open(path).read().splitlines()
        for i in raw:
            erg_dict[i] = i
    return erg_dict


if __name__ == "__main__":
    if len(sys.argv) != 3:
        sys.exit(1)
    erg_dict = {}
    erg_dict = read(sys.argv[1], erg_dict)
    erg_dict = read(sys.argv[2], erg_dict)
    for i in erg_dict:
        print(i)
