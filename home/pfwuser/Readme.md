# Access Client host/port via srtunnel

## Setup

You have to setup your client, provide a unique client config and bring it
online. To grand an user access to a client-service, first generate an unique
openssh key-pair for this, add the public key to the authorized key-file and
add the `deliver_template` so the new configline looks like this:

```
#command                             forwardedport clienthost hostport localport",no-agent-forwarding,no-X11-forwarding pubkey
command="exec /home/pfwuser/bin/deliver_port2 4001 192.168.178.254 443 2000",no-agent-forwarding,no-X11-forwarding ssh-ed25519 AA...= comment
```

This will grand you access via the forwarded port 4001 to the
client network-host on port and forwarded this to the client as port 2000.

## Use

Te user has to run:

```
ssh -i unique-cs-key pfwuser@pfwhost
```

This prepare all the necesary tasks and inform the user about the forwarded
port.

To provide this service for local use, the user has to run:

```
ssh -i unique-cs-key -L 2000:localhost:2000 pfwuser@pfwhost
```

### Comment

- To access services not direct on the client, the client settings must allow
this. By default, only client-local services are allowed.

- All client-service secutity settings are still active.
