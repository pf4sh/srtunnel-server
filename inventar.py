#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
# import os
import sys
import sqlite3
import json
import time

# TODO handle BPORTS not static
BPORTS = [8080, 8081, 8082, 8083]
START_PORT = 7000


class inv_db():
    def __init__(self, name, step=2, start_port=START_PORT):
        self.min_port = 3002
        self.max_port = 64000
        self.min_step = 2
        self.max_step = 8
        self.db = name
        if step > self.max_step:
            step = self.max_step
        if step < self.min_step:
            step = self.min_step
        self.step = step
        if start_port < self.min_port:
            start_port = self.min_port
        if start_port > self.max_port:
            start_port = self.max_port
        self.ssh_start_port = start_port

    def _mk_table_(self, cur):
        cstr = "CREATE TABLE inventar "
        cstr += "(hash TEXT PRIMARY KEY, "
        cstr += "ts TIMESTAMP, ssh_port INTEGER, web_port INTEGER)"
        cur.execute(cstr)
        cstr = "CREATE TABLE config(hash TEXT, key TEXT, value TEXT)"
        cur.execute(cstr)

    def write(self, hhash, alias='', config_list=[]):
        """
        hhash is a sha1sum hash-string
        config_list is a list of key, value dictionarys
        """
        con = None
        try:
            con = sqlite3.connect(self.db)
            cur = con.cursor()
            cur.execute('SELECT SQLITE_VERSION()')
            data = cur.fetchone()
            cur.execute("SELECT sql FROM sqlite_master WHERE name='inventar'")
            data = cur.fetchone()
            if data is None:
                self._mk_table_(cur)
        except sqlite3.Error as serr:
            sys.stderr.write(f"error: {serr}, {self.db}\n")
            con.close()
            return 0
        finally:
            if con:
                try:
                    cur.execute("SELECT MAX(ssh_port) FROM inventar")
                    last = cur.fetchone()[0]
                except Exception as e:
                    last = None
                if last is None:
                    ssh_port = self.ssh_start_port
                    web_port = ssh_port + 1
                else:
                    ssh_port = last + self.step
                    web_port = ssh_port + 1
                ok = False
                # Test of not blocked Ports
                for offset in range(self.max_port):
                    ssh_port += offset
                    web_port += offset
                    portlist = [ssh_port, web_port]
                    if self.validate_ports(portlist, self.step):
                        ok = True
                        break
                if ok:
                    istr = "INSERT INTO inventar (hash, ts, ssh_port, web_port) "
                    istr += f"VALUES('{hhash}', '{time.time()}', '{ssh_port}', '{web_port}')"
                    # print(istr)
                    try:
                        cur.execute(istr)
                    except sqlite3.OperationalError as e:
                        sys.stderr.write(f"error: db->{e}\n")
                        con.close()
                        return 0
                    except sqlite3.IntegrityError:
                        sys.stderr.write("allready registered\n")
                        con.close()
                        return 1
                    if len(config_list) == 0:
                        if alias == '':
                            alias = f"rclient_{ssh_port}"
                        config_list.append({'key': 'alias', 'value': alias})
                    for i in config_list:
                        key = i["key"]
                        value = i["value"]
                        icstr = f"INSERT INTO config VALUES('{hhash}', '{key}', '{value}')"
                        cur.execute(icstr)
                    con.commit()
                    con.close()
                    return 1
                else:
                    return 0

    def validate_ports(self, portlist, step):
        t = []
        start = min(portlist)
        for i in range(start, start+step, 1):
            portlist.append(i)
        for i in portlist:
            if i not in BPORTS:
                t.append(True)
            else:
                t.append(False)
        if False in t:
            return False
        else:
            return True

    def read(self, hhash):
        con = None
        try:
            config = {}
            con = sqlite3.connect(self.db)
            cur = con.cursor()
            qstr = "SELECT * FROM inventar WHERE hash='%s'" % hhash
            try:
                cur.execute(qstr)
            except Exception as e:
                print(f"e1: {e}")
            d0 = cur.fetchone()
            if d0 is not None:
                config['name'] = str(d0[0])
                config['ts'] = d0[1]
                config['rports'] = {}
                config['rports']['RSSH'] = d0[2]
                config['rports']['RHTTP'] = d0[3]

            qstr = "SELECT * FROM config WHERE hash='%s'" % hhash
            cur.execute(qstr)
            c = cur.fetchall()
            con.close()
            if c != []:
                config['config'] = {}
                for i in c:
                    config['config'][str(i[1])] = str(i[2])
            if config.keys() == []:
                config = ""
            else:
                config = json.dumps(config, indent=True)
        except sqlite3.Error as rerr:
            print("%s" % rerr.args[0])
            config = ""
        return json.loads(config)

    def query(self, hhash):
        con = None
        try:
            conf = {}
            con = sqlite3.connect(self.db)
            cur = con.cursor()
            qstr = "SELECT * FROM inventar WHERE hash LIKE '%" + hhash +"%'"
            try:
                cur.execute(qstr)
            except Exception as e:
                print(f"e2: {e}")
            d = cur.fetchall()
            config = []
            for i in d:
                out = []
                qstr = "SELECT * FROM config WHERE hash='%s'" % i[0]
                cur.execute(qstr)
                c = cur.fetchall()
                if c != []:
                    for j in c:
                        conf[str(j[1])] = str(j[2])
                if conf.keys() != []:
                    alias = conf['alias']
                else:
                    alias = ''
                # Alias, First_Port, Hash, ...
                out.append(alias)
                i = list(i)
                out.append(str(i.pop(2)))
                out.append(str(i.pop(0)))
                for k in i:
                    out.append(str(k))
                config.append(' '.join(out))
            con.close()
        except sqlite3.Error as rerr:
            print("%s" % rerr.args[0])
            config = ""
        return config


    def dump(self):
        con = sqlite3.connect(self.db)
        for line in con.iterdump():
            print(line)

    def get_alias(self, hhash):
        try:
            con = sqlite3.connect(self.db)
            cur = con.cursor()
            qstr = "SELECT * FROM config WHERE hash='%s'" % hhash
            cur.execute(qstr)
            c = cur.fetchone()
            if c[0] == hhash and c[1] == 'alias':
                alias = c[2]
            else:
                alias = ''
            con.close()
        except:
            alias = ''
            pass
        return alias


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog=sys.argv[0])
    parser.add_argument("hashs", help='Hash with or without alias infos, or alias')
    parser.add_argument('-r', action='store_true',
                        help='register new RClient')
    parser.add_argument('-db',
                        help='database file, default=./_inventar.db',
                        default='./_inventar.db')
    parser.add_argument('-dump', action='store_true',
                        help='dump database to stdout')

    args = parser.parse_args()
    if args.dump:
        idb = inv_db(args.db)
        idb.dump()
        sys.exit()

    idb = inv_db(args.db)
    if 'conf_' in args.hashs:
        srl = args.hashs.split('_')
        hash = srl[6].split('.')[0]
        plant = []
        plant.append(srl[1])
        plant.append(srl[2])
        plant.append(srl[3])
        plant.append(srl[4])
        alias = '_'.join(plant)
    else:
        hash = args.hashs
        alias = ''

    if len(hash) < 40 or len(hash) > 40:
        config = idb.query(hash)
        print('\n'.join(config))
        sys.exit(1)
    else:
        if args.r:
            r = idb.write(hash, alias)
            if r:
                c = idb.read(hash)
                if c == '':
                    sys.exit(2)
                else:
                    cj = json.dumps(c, indent=True)
                    try:
                        open(f"{hash}.json", "w").write(cj)
                    except Exception:
                        pass
                    print(cj)
                sys.exit(0)
            else:
                sys.exit(3)
        else:
            c = idb.read(hash)
            if c == '':
                sys.exit(2)
            else:
                cj = json.dumps(c)
                print(cj)
                sys.exit(0)
