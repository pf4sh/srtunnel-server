import json
import std/[os, strutils, strformat, base64, streams]
import struct

proc validate(pub: string): bool =
  var kptype, ptype, key, comment: string
  if pub.len() > 1000:
    return false
  let r = pub.split()
  ptype = r[0]
  key = r[1]
  comment = r[2]

  let
    ll = key.len()
    data = decode(key)
    int_len = 4
    str_len = unpack(">I", data[0 .. int_len])[0].getInt()
  kptype = data[int_len .. int_len + str_len]
  assert ptype == ptype
  if ptype != ptype:
    echo("type not valid")
  if ptype == "ssh-rsa" and ll < 372:
    stderr.writeLine("error: no rsa key")
    return false
  if ptype == "ssh-ed25519" and ll < 68:
    stderr.writeLine("error: no ed25519 key")
    return false
  return true


when isMainModule:
  let
    sufix = ".json"
    authfile = "/etc/ssh/authorized_keys/gwdata"
  var
    cflist: seq[string]
    lhash, mac, pub: string
  for kind, path in walkDir("/etc/srtunnel/"):
    case kind:
      of pcFile:
        if path.endsWith(sufix):
          cflist.add(path)
      else:
        discard path
  if paramCount() < 3:
    echo("keytype key comment")
    quit(1)
  assert paramStr(2).len() < 380
  try:
    lhash = paramStr(3)[0 .. 39]
    mac = paramStr(3)[40 .. 56]
    pub = fmt"{paramStr(1)} {paramStr(2)} {lhash}-{mac}"
  except IndexError:
    quit(4)
  let valid = validate(pub)
  if valid == false:
    quit(3)
  var
    cc = false
    cconfig: JsonNode
    rv = 9
  for i in cflist:
    var fnh = splitPath(i)[1].split(".json")[0]
    if lhash == fnh:
      try:
        let flc = readFile(i)
        cconfig = parseJson(flc)
        cc = true
        break
      except:
        cc = false
  if valid == true and cc == true:
    var ain = false
    try:
      var auth = readFile(authfile)
      var keys = auth.splitlines()
      for key in keys:
        if key.split()[0] == "#":
          continue
        if pub.split()[1] == key.split()[2]:
          ain = true
          break
    except IndexError:
      discard
    except IOError:
      discard
    if ain == false:
      let
        crssh = cconfig["rports"]["RSSH"]
        crhttp = cconfig["rports"]["RHTTP"]
        al = fmt("command=\"date\",permitlisten=\"{crssh}\",permitlisten=\"{crhttp}\" {pub}")
      try:
        var authf = openFileStream(authfile, fmAppend, -1)
        authf.writeLine(al)
        authf.close()
        rv = 0
        stderr.writeLine("info: add config")
      except:
        stderr.writeLine("error: can't add config")
        rv = 2
    else:
      rv = 1
  quit(rv)
  # nim c -d:release --opt:size --passL:-static get_srtunnel_conf.nim
