#!/usr/bin/python3
import sys
import os
from glob import glob
import json
import base64
import struct


def validate(pub):
    msg = ""
    if len(pub) > 1000:
        return False, "len"
    ptype, key, comment = pub.split()
    ll = len(key)
    data = base64.decodebytes(key.encode())
    int_len = 4
    str_len = struct.unpack(">I", data[:int_len])[0]
    out = ""
    if data[int_len:int_len+str_len].decode() != ptype:
        msg = "type error"
        s = False
    else:
        # print(ptype, l)
        if ptype == "ssh-rsa" and ll < 372:
            msg = "len error rsa"
            return False, out
        elif ptype == "ssh-ed25519" and ll < 68:
            msg = "len error ed"
            return False, msg
        s = True
    return s, msg


if __name__ == "__main__":
    """
    parameter:
    - hash from the ssh_host_ras_key.pub
    - host unique pub-key
    if hash is in inventar and pub valid, insert pubkey and send:
    - host config
    """
    debug = False
    # debug = True
    sufix = ".json"
    user = "gwdata"
    authfile = f"/etc/ssh/authorized_keys/{user}"
    dirnames = []
    out = 255
    if os.path.isdir("/etc/srtunnel"):
        dirnames.append("/etc/srtunnel/")
    cflist = []
    for dirname in dirnames:
        flist = glob(f"{dirname}*{sufix}")
        for i in flist:
            cflist.append(i)
    try:
        lhash = sys.argv[3][:40]
        mac = sys.argv[3][40:58]
        pub = f"{sys.argv[1]} {sys.argv[2][:1000]} {lhash}-{mac}"
        val, msg = validate(pub)
        for i in cflist:
            if lhash in i:
                try:
                    flc = open(f"/etc/srtunnel/{lhash}.json").read()
                    cconfig = json.loads(flc)
                    if debug:
                        print(json.dumps(cconfig, indent=True), file=sys.stdout)
                    cc = True
                    break
                except Exception as ec:
                    cc = False
                    out = 3
                    if debug:
                        print(ec, file=sys.stderr)
        if val and cc:
            ain = False
            try:
                auth = open(authfile, "r")
                keys = auth.read().splitlines()
                auth.close()
                for key in keys:
                    if key.split()[0] == "#":
                        continue
                    if debug:
                        print(key.split())
                        print("-"*80)
                    if pub.split()[1] in key.split()[3]:
                        ain = True
                        break
                if not ain:
                    crssh = cconfig["rports"]["RSSH"]
                    crhttp = cconfig["rports"]["RHTTP"]
                    auth = open(authfile, "a")
                    al = f'command="date",permitlisten="{crssh}",permitlisten="{crhttp}" {pub}\n'
                    auth.write(al)
                    out = 0
                else:
                    out = 1
                    ain = False
                    if debug:
                        print("already registered!!", file=sys.stderr)
                auth.close()
            except Exception as auth_err:
                out = 2
                if debug:
                    print(f"write pub {auth_err}", file=sys.stderr)

    except Exception as e:
        out = 4
        if debug:
            print(e, file=sys.stderr)
            print(sys.argv)
            ok = True
            for i in cflist:
                try:
                    flc = open(i).read()
                    c = json.loads(flc)
                except Exception as err:
                    ok = False
                    print(f"in {i} {err}")
            if ok:
                print("syntax check passed")
    sys.exit(out)

"""
# /home/gwuser/.ssh/authorized_keys
command="sudo -u gwdata /usr/bin/get_srtunnel_conf ${SSH_ORIGINAL_COMMAND:0:1000}",no-agent-forwarding,no-pty,no-X11-forwarding ssh-rsa AAAAB3Nz...= default.pub

# /etc/sudoers
Defaults:gwuser !requiretty
gwuser ALL=(gwdata) NOPASSWD: /usr/bin/get_srtunnel_conf
"""
